package main

import (
	"encoding/hex"
	"fmt"
	"gitlab.com/b1-201/gofare/rc522"
	"log"
	"time"
)

var zerokey = []uint8{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}

func main() {
	scanner, err := rc522.NewScanner(14)
	if err != nil {
		log.Fatal(err)
	}
	defer scanner.Close()

	fmt.Printf("Got version %X\n", scanner.ReadRegister(rc522.RegVersion))

	// Try to talk to a card
	for i := 0; i < 10; i++ {
		err = scanner.ConnectCard()
		if err == nil {
			break
		}
		time.Sleep(500 * time.Millisecond)
	}

	if err != nil {
		scanner.Close()
		log.Fatal(err)
	}

	fmt.Printf("Successful connect\n")
	fmt.Printf("nuid: %v\n", scanner.Nuid)

	// Auth
	err = scanner.AuthOnDevice(rc522.CardAuthA, 8, zerokey)
	if err != nil {
		scanner.Close()
		log.Fatal(err)
	}

	fmt.Printf("Auth on block 8\n")

	// Reading block
	blk, err := scanner.ReadBlock(8)
	if err != nil {
		scanner.Close()
		log.Fatal(err)
	}
	fmt.Printf("Block 8: %s\n", hex.EncodeToString(blk))

}
