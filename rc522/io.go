package rc522

import (
	"errors"
	"github.com/stianeikeland/go-rpio"
	"time"
)

// Registers on rc522
const (
	RegCmd       = 0x01
	RegComIrq    = 0x04
	RegError     = 0x06
	RegStatus2   = 0x08
	RegFifoData  = 0x09
	RegFifoLvl   = 0x0A
	RegBitFrame  = 0x0D
	RegMode      = 0x11
	RegTxCtrl    = 0x14
	RegTxAsk     = 0x15
	RegTMode     = 0x2A
	RegTPreScale = 0x2B
	RegTLoadHi   = 0x2C
	RegTLoadLo   = 0x2D
	RegVersion   = 0x37
)

// Rc552 commands. Not to be confused with the card commands
const (
	CmdIdle       = 0
	CmdTransmit   = 4
	CmdReceive    = 8
	CmdTransceive = 12
	CmdAuth       = 0x0E
)

var (
	ErrTimeout = errors.New("card timed out")
	ErrReader  = errors.New("reader error")
)

// Read a single value from a register in the card
// Use the RegXxxx constant for selecting registers
func (s *Scanner) ReadRegister(reg uint8) uint8 {
	// Piece together the command from spec in datasheet
	data := []uint8{0x80 | (reg << 1), 0}

	rpio.SpiExchange(data)

	// Return the second byte which is response
	return data[1]
}

// Writes multiple values to a single register
func (s *Scanner) WriteRegisterMult(reg uint8, data []uint8) {
	//fmt.Printf("Writing reg: %02X, data: %v\n", reg, data)
	// Create the data byte
	reg = reg << 1
	data = append([]uint8{reg}, data...)

	// Send the data
	rpio.SpiTransmit(data...)
}

// Writes a single value to a register
func (s *Scanner) WriteRegister(reg uint8, data uint8) {
	s.WriteRegisterMult(reg, []uint8{data})
}

// Stops current command and starts io with card.
// bitsInLast specifies how manu bits of the last byte to send.
func (s *Scanner) CardIO(cmd uint8, data []uint8, bitsInLast uint8) ([]uint8, error) {
	// Reset to idle mode and clear status reg
	s.WriteRegister(RegCmd, CmdIdle)
	s.WriteRegister(RegComIrq, 0x7F)

	// Flush fifo (see data sheet)
	s.WriteRegister(RegFifoLvl, 0x80)

	// Fill fifo with data
	s.WriteRegisterMult(RegFifoData, data)

	// Set bitsInLast, limit to 3 LSB bits
	s.WriteRegister(RegBitFrame, bitsInLast&0x07)

	// Start command
	s.WriteRegister(RegCmd, cmd)

	// If transcieve, tell it to start transmission bit 7
	{
		r := s.ReadRegister(RegBitFrame)
		s.WriteRegister(RegBitFrame, r|0x80)
	}

	var status uint8
	tries := 100
	// Wait til we are done TODO listen to ctx
	for {
		status = s.ReadRegister(RegComIrq)

		if status&0x30 > 0 {
			break
		}

		// Check the timeout
		if status&0x01 > 0 || tries <= 0 {
			return nil, ErrTimeout
		}
		tries--
		// Wait a bit
		time.Sleep(1 * time.Millisecond)
	}

	// TODO check error in REG_ERROR
	if s.ReadRegister(RegError) > 0 {
		return nil, ErrReader
	}

	// Receive
	var buff []uint8
	if cmd == CmdReceive || cmd == CmdTransceive {
		// Read waiting bytes
		fifolvl := s.ReadRegister(RegFifoLvl)

		buff = make([]uint8, fifolvl)
		var i uint8
		for i = 0; i < fifolvl; i++ {
			buff[i] = s.ReadRegister(RegFifoData)
		}
	}

	return buff, nil

}
