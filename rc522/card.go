package rc522

import (
	"errors"
	"fmt"
)

var (
	ErrInvalidResp     = errors.New("Invalid card response")
	ErrInvalidBlock    = errors.New("Invalid block address")
	ErrInvalidCrc      = errors.New("Invalid crc in response")
	ErrInvalidDataSize = errors.New("Invalid data size, does not match block size")
	ErrCardFailure     = errors.New("Card returned NAK")
)

// Mifare classic card commands
const (
	CardRead  = 0x30
	CardWrite = 0xA0 // Writes one 16 byte block to an authenticated sector.
	CardAuthA = 0x60
	CardAuthB = 0x61
)

// Mifare classic card responses
const (
	CardACK = 10
)

func (s *Scanner) sendReqA() error {
	cmd := []uint8{0x26}
	resp, err := s.CardIO(CmdTransceive, cmd, 7)
	if err != nil {
		return err
	}
	if len(resp) != 2 {
		return ErrInvalidResp
	}

	if resp[0] == 0 && resp[1] == 0x04 {
		return ErrInvalidResp
	}

	return nil
}

func (s *Scanner) selectCard() error {
	// Do anticollision CL 1
	cmd := []uint8{0x93, 0x20}

	rsp, err := s.CardIO(CmdTransceive, cmd, 0)
	if err != nil {
		return err
	}
	if len(rsp) != 5 {
		return ErrInvalidResp
	}

	// Save the nuid for later
	s.Nuid = append([]uint8{}, rsp[0:4]...)

	// Do a select
	cmd = []uint8{0x93, 0x70}
	cmd = append(cmd, rsp...)

	// Add crc
	cmd = append(cmd, calcCRC(cmd[0:7])...)
	rsp, err = s.CardIO(CmdTransceive, cmd, 0)
	if err != nil {
		return err
	}
	if len(rsp) != 3 && rsp[0] == 0x08 {
		return ErrInvalidResp
	}

	return nil
}

// Uses the Rc552 onboard crypto engine for authorication on a block
func (s *Scanner) AuthOnDevice(cmd uint8, addr uint8, key []uint8) error {
	// Build package
	pack := make([]uint8, 2, 12)
	pack[0] = cmd
	pack[1] = addr

	// Copy key
	pack = append(pack, key...)

	// Copy uid
	pack = append(pack, s.Nuid...)

	fmt.Printf("Sending auth: %v\n", pack)
	_, err := s.CardIO(CmdAuth, pack[:], 0)

	return err
}

func (s *Scanner) ReadBlock(addr uint8) ([]uint8, error) {
	if addr > 63 {
		return nil, ErrInvalidBlock
	}

	// Prepare command and send
	cmd := []uint8{CardRead, addr}
	cmd = append(cmd, calcCRC(cmd)...)
	rsp, err := s.CardIO(CmdTransceive, cmd, 0)
	if err != nil {
		return nil, err
	}

	if len(rsp) < 18 {
		return nil, ErrInvalidResp
	}

	// Check crc
	if !verifyCRC(rsp) {
		err = ErrInvalidCrc
	}

	// Last thing we do so we dont need check
	return rsp[:len(rsp)-2], err
}

func checkACK(code uint8) error {
	if code != CardACK {
		// Different types of NAK, see datasheet Table 10.
		switch code {
		case 0:
			return errors.New("card rsp: invalid operation")
		case 1:
			return errors.New("card rsp: parity or CRC error")
		case 4:
			return errors.New("card rsp: invalid operation")
		case 5:
			return errors.New("card rsp: parity or CRC error")
		default:
			return errors.New("card rsp: unknown non-ACK code")
		}
	}
	return nil
}

func (s *Scanner) WriteBlock(block uint8, data []uint8) error {
	block_size := 16
	if len(data) != block_size {
		return ErrInvalidDataSize
	}

	// Part 1: send "i am about to write"
	cmd := []uint8{CardWrite, block}
	cmd = append(cmd, calcCRC(cmd)...)
	rsp, err := s.CardIO(CmdTransceive, cmd, 0)
	if err != nil {
		return fmt.Errorf("Part 1.a: %v", err)
	}

	if len(rsp) == 0 {
		return fmt.Errorf("Part 1.b: %v", ErrInvalidResp)
	}
	err = checkACK(rsp[0])
	if err != nil {
		return fmt.Errorf("Part 1.c: %v", err)
	}

	// Part 2: send data to be written
	data = append(data, calcCRC(data)...)
	rsp, err = s.CardIO(CmdTransceive, data, 0)
	if err != nil {
		return fmt.Errorf("Part 2.a: %v", err)
	}

	if len(rsp) == 0 {
		return fmt.Errorf("Part 2.b: %v", ErrInvalidResp)
	}

	err = checkACK(rsp[0])
	if err != nil {
		return fmt.Errorf("Part 2.c: %v", err)
	}

	return nil
}

// Copied from libnfc/iso14443-subr.c
func calcCRC(input []uint8) []uint8 {
	var wCrc uint32 = 0x6363

	for _, bt := range input {
		bt = (bt ^ uint8(wCrc&0x00FF))
		bt = (bt ^ (bt << 4))
		wCrc = (wCrc >> 8) ^ (uint32(bt) << 8) ^ (uint32(bt) << 3) ^ (uint32(bt) >> 4)
	}

	return []uint8{uint8(wCrc & 0xFF), uint8((wCrc >> 8) & 0xFF)}
}

// Checks if the last two bytes of input are the correct CRC
func verifyCRC(input []uint8) bool {
	crc := calcCRC(input[:len(input)-2])

	return crc[0] == input[len(input)-2] && crc[1] == input[len(input)-1]
}

func byteFromBin(bin string) byte {
	var res byte
	for i, b := range bin {
		if b == '1' {
			res = res | (1 << uint(7-i))
		}
	}
	return res
}

func GenKeyBlock(new_key []byte) ([]byte, error) {
	// flipbit
	fB := func(bit string) string {
		if bit == "1" {
			return "0"
		} else if bit == "0" {
			return "1"
		}
		return ""
	}

	// Assumes already authed
	if len(new_key) != 6 {
		return nil, errors.New("Key length is not 6 bytes")
	}
	/*
	  format: <CN_M>
	  C = C
	  N = meaning value (see tables in datasheet)
	  M = block index in sector
	*/

	// Block 0 access
	C1_0 := "0"
	C2_0 := "0"
	C3_0 := "0"

	// Block 1
	C1_1 := "0"
	C2_1 := "0"
	C3_1 := "0"

	// Block 2
	C1_2 := "0"
	C2_2 := "0"
	C3_2 := "0"

	// Block 3 (sector trailer)
	C1_3 := "0"
	C2_3 := "0"
	C3_3 := "1"

	byte6 := fB(C2_3) + fB(C2_2) + fB(C2_1) + fB(C2_0) + fB(C1_3) + fB(C1_2) + fB(C1_1) + fB(C1_0)

	byte7 := C1_3 + C1_2 + C1_1 + C1_0 + fB(C3_3) + fB(C3_2) + fB(C3_1) + fB(C3_0)

	byte8 := C3_3 + C3_2 + C3_1 + C3_0 + C2_3 + C2_2 + C2_1 + C2_0

	access_bytes := []byte{byteFromBin(byte6), byteFromBin(byte7), byteFromBin(byte8)}

	// Assemble stuff to be written
	key_block := make([]byte, 0)

	// Key A
	for _, key_byte := range new_key {
		key_block = append(key_block, key_byte)
	}

	// Access bytes
	for _, access_byte := range access_bytes {
		key_block = append(key_block, access_byte)
	}

	// user data (unused and therefore 0)
	key_block = append(key_block, 0x00)

	// Key B
	for _, key_byte := range new_key {
		key_block = append(key_block, key_byte)
	}
	return key_block, nil
}
