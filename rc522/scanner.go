// Implements RC522 operations on raspberry pi
package rc522

import (
	"github.com/stianeikeland/go-rpio"
	"time"
)

type Scanner struct {
	rst  rpio.Pin
	Nuid []uint8 // nuid of last selected card
}

// Creates a new scanner using the specified rstPin
func NewScanner(rstPin uint) (*Scanner, error) {
	err := rpio.Open()
	if err != nil {
		return nil, err
	}

	// Start the spi stuff
	err = rpio.SpiBegin(rpio.Spi0)
	if err != nil {
		return nil, err
	}

	rpio.SpiSpeed(4000000)

	// Setup pin
	s := new(Scanner)
	s.rst = rpio.Pin(rstPin)
	s.rst.Mode(rpio.Output)
	s.rst.High()

	time.Sleep(10 * time.Millisecond)

	// Setup registers
	s.setupTimeout()
	s.WriteRegister(RegTxAsk, 0x40) // Turn on ASK modulation
	s.WriteRegister(RegMode, 0x3D)  // Set correct mode rec for internal CRC

	// Turn on antenna
	s.setAntenna(true)

	return s, nil
}

// Remember to close when done
func (s *Scanner) Close() {

	s.rst.Low()
	rpio.SpiEnd(rpio.Spi0)
	rpio.Close()
}

// Send reqa, anticollison and select
func (s *Scanner) ConnectCard() error {
	err := s.sendReqA()
	if err != nil {
		return err
	}

	err = s.selectCard()
	if err != nil {
		return err
	}

	return nil
}

func (s *Scanner) setAntenna(on bool) {
	state := s.ReadRegister(RegTxCtrl)

	// Apply change
	if on {
		state = state | 0x03
	} else {
		state = state & (^uint8(0x03))
	}

	// Write back
	s.WriteRegister(RegTxCtrl, state)
}

// TODO make parameter
func (s *Scanner) setupTimeout() {
	// Setup 25 ms timeout see (https://github.com/miguelbalboa/rfid/blob/b7d7f3e572af6cfd78722f2bd693097019ec55ff/src/MFRC522.cpp#L229)
	s.WriteRegister(RegTMode, 0x80)
	s.WriteRegister(RegTPreScale, 0xA6) // increment_time = 25 us
	s.WriteRegister(RegTLoadHi, 0x07)   // 0x03D0 = 2000, thus 2000 increments before timeout
	s.WriteRegister(RegTLoadLo, 0xD0)
}

func (s *Scanner) CloseAuth() {
    state := s.ReadRegister(RegStatus2)
    state = state & 0xF7
    s.WriteRegister(RegStatus2, state)
}

