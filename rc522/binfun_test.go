package rc522

import "testing"

func TestBinFunc(t *testing.T) {
  test := func(input string, output byte) {
    if b := byteFromBin(input); b != output {
      t.Errorf("got %X from %s, expected %X", b, input, output)
    }
  }

  test("00000000", 0x00)
  test("11111111", 0xFF)
  test("10101011", 0xAB)
  test("10111011", 0xBB)
  test("11100011", 0xE3)
  test("00011101", 0x1D)
  test("11010011", 0xD3)
}
