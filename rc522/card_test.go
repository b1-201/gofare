package rc522

import (
	"reflect"
	"testing"
)

func TestCRC(t *testing.T) {
	rsp := calcCRC([]uint8{0x60, 0x30})
	if !reflect.DeepEqual(rsp, []uint8{118, 74}) {
		t.Errorf("Expected [118 74] got %v", rsp)
	}

	rsp = calcCRC([]uint8{147, 112, 1, 178, 51, 46, 174})
	if !reflect.DeepEqual(rsp, []uint8{175, 22}) {
		t.Errorf("Expected [175 22] got %v", rsp)
	}
}

func TestVerifyCRC(t *testing.T) {
	if !verifyCRC([]uint8{0x60, 0x30, 118, 74}) {
		t.Errorf("Expected pass on [0x60, 0x30, 118, 74]")
	}
	if !verifyCRC([]uint8{147, 112, 1, 178, 51, 46, 174, 175, 22}) {
		t.Errorf("Expected pass on [147, 112, 1, 178, 51, 46, 174, 75, 22]")
	}
	if verifyCRC([]uint8{147, 112, 1, 178, 46, 174, 175, 22}) {
		t.Errorf("Expected false on [147, 112, 1, 178, 46, 174, 75, 22]")
	}
}
