package crypt

// Interface to generalize interface for crypto operations.
// This makes moving crypto operations to other devices easy
// 
// Cipher takes two inputs one for feedin and the data to be de/encrypted.
// It returns the output, parity bits and potential errors
// 
// Cipherword in crapto does some wierd stuff, TODO infer this from Cipher
type Cipher interface {
	Cipher(feedin []uint8, input []uint8) ([]uint8, []uint8, error)
}

func (cs *CryptState) Cipher(feedin []uint8, input []uint8) ([]uint8, []uint8, error) {
	l := len(input)
	if input == nil {
		l = len(feedin)
	}
	output := make([]uint8, l)
	parity := make([]uint8, l)
	for i := 0; i < l; i++{
		var feed, in uint8
		if feedin != nil {
			// Check if feedin is nil
			feed = feedin[i]
		}
		if input != nil {
			in = input[i]
		}
		output[i] = cs.CipherByte(feed, false) ^ in
		parity[i] = Filter( cs.Odd ) ^ OddParity( in )
	}
	return output, parity, nil
}

func OddParity(bt byte) byte {
	// cf http://graphics.stanford.edu/~seander/bithacks.html#ParityParallel
	// Black magic and magnets
	var temp uint16 = (0x9669 >> ((bt ^(bt >> 4)) & 0xF)) & 1;
	return uint8(temp);
}

