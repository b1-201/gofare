// Port of crapto1
package crypt

type CryptState struct {
	Odd, Even uint32
}

func NewCrypt(key []uint8) *CryptState {
	cs := new(CryptState)

	// For every byte in key
	for _, b := range key {
		// For every bit
		Even := false
		for i := 0; i < 8; i++ {
			bit := uint32((b >> i) & 0x01)
			if Even {
				cs.Odd = (cs.Odd << 1) | bit
			} else {
				cs.Even = (cs.Even << 1) | bit
			}
			Even = !Even
		}
	}

	return cs
}

func (cs *CryptState) CipherBit(in uint8, is_enc bool) uint8 {
	var feedin uint32
	var ret uint8 = Filter(cs.Odd)

	if is_enc {
		feedin = uint32(ret & 1)
	}
	if in > 0 {
		feedin ^= 1
	}
	feedin ^= 0x29CE5C & cs.Odd
	feedin ^= 0x870804 & cs.Even

	cs.Even = (cs.Even << 1) | uint32(Parity(feedin))

	cs.Odd ^= cs.Even
	cs.Even ^= cs.Odd
	cs.Odd ^= cs.Even

	return ret
}

func (cs *CryptState) CipherByte(in uint8, is_enc bool) uint8 {
	var ret uint8
	for i := 0; i < 8; i++ {
		ret |= cs.CipherBit((in>>i)&1, is_enc) << i
	}

	return ret
}

func (cs *CryptState) CipherWord(in uint32, is_enc bool) uint32 {
	var ret uint32
	for i := 0; i < 32; i++ {
		// Okaaaya https://github.com/HakonHystad/MFRC522_nested_attack/blob/211ed2a64ac5658e181b1a5cbf5bc63e9cc90775/src/crypto1.c#L78
		ret |= uint32(cs.CipherBit(uint8(in>>(i^24))&1, is_enc)) << (i ^ 24)
	}
	return ret
}

func Filter(x uint32) uint8 {
	var f uint32

	// Wow i have no idea what this is. Fortunatly c is kind of compatible with go
	f = 0xf22c0 >> (x & 0xf) & 16
	f |= 0x6c9c0 >> (x >> 4 & 0xf) & 8
	f |= 0x3c8b0 >> (x >> 8 & 0xf) & 4
	f |= 0x1e458 >> (x >> 12 & 0xf) & 2
	f |= 0x0d938 >> (x >> 16 & 0xf) & 1

	f = 0xEC57E80A >> f

	return uint8(f & 0x1)
}

func Parity(x uint32) uint8 {
	x ^= x >> 16
	x ^= x >> 8
	x ^= x >> 4

	x = 0x6996 >> (x & 0xf)

	return uint8(x & 1)

}

// Copied from https://github.com/HakonHystad/MFRC522_nested_attack/blob/211ed2a64ac5658e181b1a5cbf5bc63e9cc90775/src/crypto1.c#L86
func PrngSuccessor(x, n uint32) uint32 {
	swapEndian := func() {
		x = (x >> 8 & 0xff00ff) | (x&0xff00ff)<<8
		x = x>>16 | x<<16
	}

	swapEndian()

	for ; n > 0; n-- {
		x = x>>1 | (x>>16^x>>18^x>>19^x>>21)<<31
	}

	swapEndian()
	return x
}
