package crypt

import (
	"testing"
)

var testkey = []uint8{0xED, 0x56, 0x22, 0xFF, 0x34, 0xAB}

func TestNewCrypt(t *testing.T) {
	verifyState := func(cs *CryptState, Odd, Even uint32) {
		if cs.Odd != Odd {
			t.Errorf("Wrong Odd: %08X", cs.Odd)
		}
		if cs.Even != Even {
			t.Errorf("Wrong Even: %08X", cs.Even)
		}
	}

	cs := NewCrypt(testkey)

	verifyState(cs, 0x0078AF2F, 0x00D70F68)

	key := []uint8{0, 0, 0, 0, 0, 0}
	cs = NewCrypt(key)

	verifyState(cs, 0, 0)

	key = []uint8{0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC}
	cs = NewCrypt(key)

	verifyState(cs, 0x008286D7, 0x00267326)
}

func TestFilter(t *testing.T) {
	testFilter := func(in uint32, out uint8) {
		rest := Filter(in)
		if rest != out {
			t.Errorf("Filter(%X) -> %X expected %X", in, rest, out)
		}
	}

	testFilter(0, 0)
	testFilter(0x12344556, 1)
	testFilter(0x0000000A, 0)
	testFilter(0xFFFFFFFF, 1)
	testFilter(0x00005349, 1)
	testFilter(0x0D70F684, 0)
}

func TestParity(t *testing.T) {
	testParity := func(in uint32, out uint8) {
		rest := Parity(in)
		if rest != out {
			t.Errorf("Parity(%X) -> %X expected %X", in, rest, out)
		}
	}

	testParity(0, 0)
	testParity(0x12344556, 0)
	testParity(0xFFFFFFFF, 0)
	testParity(0x00005349, 1)
	testParity(0x0000000B, 1)
}

func TestPrng(t *testing.T) {
	testPrng := func(x, n uint32, out uint32) {
		rest := PrngSuccessor(x, n)
		if rest != out {
			t.Errorf("Parity(%X, %X) -> %X expected %X", x, n, rest, out)
		}
	}

	testPrng(0, 0x3E8, 0)
	testPrng(0x12344556, 0x01EEE127, 0x9A7F7354)
	testPrng(0xFFFFFFFF, 0x000000D5, 0x14A17BE0)
	testPrng(0x00005349, 0xA, 0xC054D219)
	testPrng(0xB, 0, 0xB)

}

func TestCipherBit(t *testing.T) {
	cs := NewCrypt(testkey)

	testCipher := func(in uint8, enc bool, out uint8) {
		rest := cs.CipherBit(in, enc)
		if rest != out {
			t.Errorf("CipherBit(%X) -> %X expected %X", in, rest, out)
		}
	}

	testCipher(1, false, 0)
	for i := 0; i < 10000; i++ {
		cs.CipherBit(uint8(i), false)
	}
	testCipher(1, false, 1)
	testCipher(0, false, 0)
	testCipher(1, true, 0)

	testCipher(1, false, 1)

}

func TestCipherByte(t *testing.T) {
	cs := NewCrypt(testkey)
	testCipher := func(in uint8, enc bool, out uint8) {
		rest := cs.CipherByte(in, enc)
		if rest != out {
			t.Errorf("CipherByte(%X) -> %X expected %X", in, rest, out)
		}
	}

	testCipher(0x55, false, 0x04)
	testCipher(0xAB, false, 0x5D)
	testCipher(0xF4, true, 0x8E)

	// Let it run for at while
	for i := 0; i < 10000; i++ {
		cs.CipherByte(uint8(i), false)
	}
	testCipher(0xB1, false, 0xCD)
	testCipher(0xFF, true, 0x70)
}

func TestCipherWord(t *testing.T) {

	cs := NewCrypt(testkey)
	testCipher := func(in uint32, enc bool, out uint32) {
		rest := cs.CipherWord(in, enc)
		if rest != out {
			t.Errorf("CipherWord(%08X) -> %08X expected %08X", in, rest, out)
		}
	}

	testCipher(0x55123411, false, 0x477A4A5)
	testCipher(0xAB00FFD2, false, 0x621386B)
	testCipher(0xF4000000, true, 0xF43B9A9A)

	// Let it run for at while
	for i := 0; i < 10000; i++ {
		cs.CipherWord(uint32(i), false)
	}
	testCipher(0x003454FF, false, 0x257C9D9)
	testCipher(0x12F1B499, true, 0x57E2D30F)
}
